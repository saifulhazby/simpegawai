<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmyeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emyees')->insert([
            'nip' => '111',
            'nama' => 'hani',
            'email' => 'tahu@gmail.com',
            'jeniskelamin' => 'wanita',
            'notelpon' => '087755228507',
            'alamat' => 'sampang'
        ]);
    }
}
