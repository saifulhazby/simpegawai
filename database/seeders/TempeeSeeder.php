<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TempeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tempees')->insert([
            'nip' => '111',
            'nama' => 'hani',
            'email' => 'tahu@gmail.com',
            'jabatan' => 'ketua',
            'notelpon' => '087755228507',
        ]);
    }
}
