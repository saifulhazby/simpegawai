<?php

use App\Models\post;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmyeeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TempeeController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DashboardPostController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return view('/login/index', [
       "title" => "login"
    ]);
});

Route::get('/tahu', function () {
    return view('/tahu', [
        "title" => "pegawai"
     ]);
 });

//Route::get('/home', function () {
//    return view('home');
//});

//Route::get('/about', function () {
//    return view('about',[
//        "title" => "about",
//        "name" => "saiful",
//        "email" => "saifulhazby@gmail.com",
//        "image" => "tahu.jpg"
//    ]);
//});

//Route::get('/posts', function (){
//    return view('post',[
//        "title" => "post",
//    ]);
//});

//Route::get('/post', function () {
//    $blog_post = [
//        [
//            "title" => "judul post first",
//            "author" => "hazby",
//            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore mollitia maxime, voluptatum voluptas, quasi reiciendis veritatis ut unde accusantium quae aspernatur fuga quod necessitatibus vero laboriosam architecto similique cupiditate iusto voluptatibus quos ipsa tenetur, incidunt distinctio quia. Voluptates veniam iure explicabo. Rem, alias aperiam? Earum, quia cum beatae tempora, sapiente quis, facere optio in consequuntur illo eius dolorum saepe dolor quo eum nobis perferendis praesentium corrupti quas exercitationem similique! Odit blanditiis placeat itaque maxime tempore commodi ipsam ducimus nostrum adipisci!"
//        ],
//        ];

//    return view('post',[
//        "title" => "post",
//        "post" => $blog_post
//    ]);
//});


Route::get('/login', [LoginController::class, 'index'])->name('login')->Middleware('guest');

Route::post('/login', [LoginController::class, 'authenticate']);

Route::post('/logout', [LoginController::class, 'logout']);

Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');

Route::post('/register', [RegisterController::class, 'store']);

Route::get('/dashboard', function(){
    return view('dashboard.index');
});
//->middleware('auth');

Route::resource('/dashboard/post', DashboardPostController::class);
//->middleware('auth');

Route::get('/karyawan', [EmyeeController::class, 'index'])->name('karyawan')->middleware('auth');
Route::get('/show', [TempeeController::class, 'index'])->name('karyawan')->middleware('auth');
Route::get('/tambahdata', [EmyeeController::class, 'tambahdata'])->name('tambahdata')->middleware('auth');
Route::get('/add', [TempeeController::class, 'add'])->name('add')->middleware('auth');
Route::post('/in', [TempeeController::class, 'in'])->name('in')->middleware('auth');
Route::post('/insertdata', [EmyeeController::class, 'insertdata'])->name('insertdata')->middleware('auth');
Route::get('/tamdata/{id}', [EmyeeController::class, 'tamdata'])->name('tamdata')->middleware('auth');
Route::post('/updata/{id}', [EmyeeController::class, 'updata'])->name('updata')->middleware('auth');
Route::get('/del/{id}', [EmyeeController::class, 'del'])->name('del');
Route::get('/delete/{id}', [TempeeController::class, 'delete'])->name('delete');

//export pdf
Route::get('/export', [EmyeeController::class, 'export'])->name('export');