<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PegawaiModel extends Model
{
    public function allData()
    {
        return DB::table('tbl_pegawai')->get();
    }

    public function detailData($id)
    {
        return DB::table('tbl_pegawai')->where('id', '$id')->first();
    }

}
