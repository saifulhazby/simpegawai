<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index(){
        return view('register.index',[
            'title' => 'Register',
            'active' => 'register'
        ]);
    }
    public function store(Request $request)
   {
//       return request()->all();
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:6|max:255'
        ]);

        //dd('berhasil');

        //$validatedData['password'] =bcrypt($validatedData['password']);

        $validatedData['password'] = Hash::make($validatedData['password']);

        user::create($validatedData);

        //$request->session()->flash('success', 'registration successfull! please login');

        return redirect('/login')->with('success', 'registration successfull! please login');
//
    }
}
