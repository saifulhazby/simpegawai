<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PegawaiModel;

class PegawaiController extends Controller
{
    public function __construct()
    {
        $this->PegawaiModel = new PegawaiModel();
    }

    public function index()
    {
        $data = [
            'pegawai' => $this->PegawaiModel->allData(),
        ];
        return view('master', $data);
    }

    public function detail($id)
    {
        $data = [
            'pegawai' => $this->PegawaiModel->detailData('$id'),
        ];
        return view('detail', $data);
    }
}
