<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tempee;

class TempeeController extends Controller
{
    public function index()
    {
        $data = tempee::all();
        return view('show', compact('data'));
    }
    
    public function add()
    {
        return view('add');
    }
    
    public function in(Request $request)
    {
        Tempee::create($request->all());
        return redirect()->route('karyawan')->with('success','succsessfuly');
    }

    public function delete($id)
    {
        $data = Tempee::find($id);
        $data->delete();
        return redirect()->route('karyawan')->with('success','data berhasil dihapus');
    }
}
