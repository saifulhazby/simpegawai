<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Emyee;
use PDF;

class EmyeeController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $data = emyee::where('nama','LIKE','%'.$request->search.'%');
        }else{

            $data = emyee::paginate(5);
        }


        return view('data', compact('data'));
    }
    

    public function tambahdata()
    {
        return view('tambahdata');
    }

    public function insertdata(Request $request)
    {
        //dd($request->all());
        $data = emyee::create($request->all());
        if($request->File('foto')){
            $request->file('foto')->move('foto', $request->file('foto')->getClientOriginalName());
            $data->foto = $request->file('foto')->getClientOriginalName();
            $data->save();
        }
        return redirect()->route('add')->with('success','data berhasil ditambahkan');
    }

    public function tamdata($id)
    {
        $data = Emyee::find($id);
        //dd($data);
        return view('tamdata', compact('data'));
    }

    public function updata(Request $request, $id)
    {
        $data = Emyee::find($id);
        $data->update($request->all());
        return redirect()->route('karyawan')->with('success','data berhasil diupdate');

    }

    public function del($id)
    {
        $data = Emyee::find($id);
        $data->delete();
        return redirect()->route('karyawan')->with('success','data berhasil dihapus');
    }

    public function export()
    {
        $data = Emyee::all();

        view()->share('data', $data);
        $pdf = PDF::loadview('data-pdf');
        return $pdf->download('data.pdf');
    }
}
