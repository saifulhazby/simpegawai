@extends('dashboard.layouts.main')

@section('container')

<div class="container pt-3 pb-2 mb-3 border-bottom">
    <div class="row justify-content-center">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <form action="/insertdata" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">NIP</label>
                          <input type="number" name="nip" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                          <div id="emailHelp"</div>
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Nama Lengkap</label>
                          <input type="text" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                          <div id="emailHelp"</div>
                          </div>
                          <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                            <div id="emailHelp"</div>
                          </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Jenis Kelamin</label>
                          <select class="form-select" name="jeniskelamin" aria-label="Default select example">
                            <option selected>Pilih Jenis Kelamin</option>
                            <option value="pria">pria</option>
                            <option value="wanita">wanita</option>
                          </select>
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Notelpon</label>
                          <input type="number" name="notelpon" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                          <div id="emailHelp"</div>
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Alamat</label>
                          <input type="text" name="alamat" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                          <div id="emailHelp"</div>
                          </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Pilih Foto</label>
                          <input type="file" name="foto" class="form-control" required>
                          <div id="emailHelp"</div>
                          </div>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection