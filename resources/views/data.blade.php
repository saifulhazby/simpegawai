@extends('dashboard.layouts.main')

@section('container')

<h1 class="text-center mb-2">Data Pribadi Karyawan</h1>

<div class="container pt-3 pb-2 mb-3 border-bottom">
    <a href="/tambahdata" class="btn btn-success">tambah +</a>
    
    <div class="pt-2 pb-2 mb-2 border-bottom">
      <a href="/export" class="btn btn-info">Export PDF</a>
    </div>

    <div class="row pt-1 border-bottom">
      @if ($message = Session::get('success',))
      <div class="alert alert-success" role="alert">
        {{ $message }}
    </div>   
      @endif
      <table class="table table-dark table-striped">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Nip</th>
                <th scope="col">Nama</th>
                <th scope="col">Email</th>
                <th scope="col">Jenis kelamin</th>
                <th scope="col">No Telpon</th>
                <th scope="col">Alamat</th>
                <th scope="col">Foto</th>
                <th scope="col">Date</th>
                <th scope="col">action</th>
              </tr>
            </thead>
            <tbody>
              @php
                $no = 1;
                @endphp
                @foreach ($data as $index => $row)      
                <tr>
                  <th scope="row">{{ $index + $data->firstItem() }}</th>
                  <td>{{ $row->nip }}</td>
                  <td>{{ $row->nama }}</td>
                  <td>{{ $row->email }}</td>
                  <td>{{ $row->jeniskelamin }}</td>
                  <td>{{ $row->notelpon }}</td>
                  <td>0{{ $row->alamat }}</td>
                  <td>
                    <img src="{{ asset('foto/'.$row->foto) }}" alt="" style="width: 60px;">
                  </td>
                  <td>{{ $row->created_at->diffForHumans() }}</td>
                  <td>
                    <a href="/tamdata/{{ $row->id }}" class="btn btn-warning">edit</a>
                    <a href="/del/{{ $row->id }}" class="btn btn-danger">Delete</a>
                  </td>
                </tr>
                @endforeach
            </tbody>
          </table>
          {{ $data->links() }}
    </div>
</div>

<script>
    toastr.success('Have fun storming the castle!', 'Miracle Max Says')
</script>

@endsection