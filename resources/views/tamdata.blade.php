@extends('dashboard.layouts.main')

@section('container')

<h1 class="text-center mb-2">edit data</h1>

<div class="container pt-3 pb-2 mb-3 border-bottom">
    <div class="row justify-content-center">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <form action="/updata/{{ $data->id }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">NIP</label>
                          <input type="number" name="nip" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->nip }}">
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Nama Lengkap</label>
                          <input type="text" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->nama }}">
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Jenis Kelamin</label>
                          <select class="form-select" name="jeniskelamin" aria-label="Default select example">
                            <option selected>{{ $data->jeniskelamin }}</option>
                            <option value="pria">pria</option>
                            <option value="wanita">wanita</option>
                          </select>
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Notelpon</label>
                          <input type="number" name="notelpon" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->notelpon }}">
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Alamat</label>
                          <input type="text" name="alamat" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->alamat }}">
                          </div>
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">pilih Foto</label>
                          <input type="file" name="foto" class="form-control">
                          </div>
                        <button type="submit" class="btn btn-primary pt-3 pb-2 mb-3 border-bottom">Submit</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection