@extends('dashboard.layouts.main')

@section('container')

<h1 class="text-center mb-2">data karyawan</h1>

<div class="container pt-3 pb-2 mb-3 border-bottom">
    <div class="row pt-1 border-bottom">
        <table class="table table-dark table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nip</th>
                <th scope="col">Nama</th>
                <th scope="col">Jenis kelamin</th>
                <th scope="col">Notelpon</th>
                <th scope="col">Alamat</th>
                <th scope="col">Foto</th>
                <th scope="col">Date</th>
                <th scope="col">action</th>
              </tr>
            </thead>
            <tbody>    
                <tr>
                  <th scope="row">{{ $no++ }}</th>
                  <td>{{ $row->nip }}</td>
                  <td>{{ $row->nama }}</td>
                  <td>{{ $row->jeniskelamin }}</td>
                  <td>{{ $row->notelpon }}</td>
                  <td>{{ $row->alamat }}</td>
                  <td>
                    <img src="{{ asset('foto/'.$row->foto) }}" alt="" style="width: 60px;">
                  </td>
                  <td>{{ $row->created_at->diffForHumans() }}</td>
                </tr>
            </tbody>
          </table>
    </div>
</div>

<script>
    toastr.success('Have fun storming the castle!', 'Miracle Max Says')
</script>

@endsection