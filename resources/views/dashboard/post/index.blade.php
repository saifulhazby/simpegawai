@extends('dashboard.layouts.main')

@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
<h1 class="h2">Please Check in</h1>
</div>

<div class="table-responsive col-lg-8">
  <a href="/dashboard/post/create" class="btn btn-primary mb-3">Tambah data</a>
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Title</th>
          <th scope="col">category</th>
          <th scope="col">tanggal</th>
          <th scope="col">action</th>
        </tr>
      </thead>
      <tbody>
          <tr>
            <td>1,001</td>
            <td>random</td>
            <td>data</td>
            <td>placeholder</td>
            <td>
              <a href="/about" class="badge bg-info"><span data-feather="eye"></span></a>
              <a href="/dashboard/post" class="badge bg-warning"><span data-feather="edit"></span></a>
              <a href="/dashboard/post" class="badge bg-danger"><span data-feather="x-circle"></span></a>
            </td>
          </tr>    
      </tbody>
    </table>
  </div>

@endsection