<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="position-sticky pt-3">
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link {{ Request::is('dashboard') ? 'active' : '' }}" aria-current="page" href="/dashboard">
            <span data-feather="home"></span>
            Dashboard
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ Request::is('karyawan') ? 'active' : '' }}" href="/karyawan">
            <span data-feather="file-text"></span>
            Tabel Master
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ Request::is('show') ? 'active' : '' }}" href="/show">
            <span data-feather="users"></span>
            Pegawai
          </a>
        </li>
        <li class="nav-item">
        </li>
  </nav>