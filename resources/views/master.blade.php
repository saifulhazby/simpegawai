@extends('dashboard.layouts.main')

@section('container')

    <table class="table table-boedered">
        <thead>
            <tr>
                <th>no</th>
                <th>id</th>
                <th>nama</th>
                <th>no telpon</th>
                <th>jabatan</th>
                <th>foto</th>
                <th>action</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; ?>
            @foreach ($pegawai as $data)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $data->id }}</td>
                    <td>{{ $data->nama }}</td>
                    <td>{{ $data->no_telpon }}</td>
                    <td>{{ $data->jabatan }}</td>
                    <td><img src="{{ url('foto/'.$data->foto) }}" width="100px"></td>
                    <td>
                        <a href="/detail" class="btn btn-sm btn-success">detail</a>
                        <a href="/about" class="btn btn-sm btn-warning">edit</a>
                        <a href="" class="btn btn-sm btn-danger">delate</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection