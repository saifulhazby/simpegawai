@extends('dashboard.layouts.main')

@section('container')

<h1 class="text-center mb-2">Data Profesi Karyawan</h1>

<div class="container pt-3 pb-2 mb-3 border-bottom">
  <a href="/tambahdata" class="btn btn-primary">ADD +</a>
    <div class="row">
      @if ($message = Session::get('primary'))
          <div class="alert alert-success" role="alert">
            {{ $message }}
          </div>   
      @endif
        <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Nip</th>
                <th scope="col">Nama</th>
                <th scope="col">email</th>
                <th scope="col">Jabatan</th>
                <th scope="col">No Telpon</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
          <tbody>
            @php
                $no = 1;
            @endphp
          @foreach ($data as $row)
          <tr>
            <th scope="row">{{ $no++ }}</th>
            <td>{{ $row->nip }}</td>
            <td>{{ $row->nama }}</td>
            <td>{{ $row->email }}</td>
            <td>{{ $row->jabatan }}</td>
            <td>0{{ $row->notelpon }}</td>
                <td>
                  <a href="/delete/{{ $row->id }}" class="btn btn-danger">Delete</a>
                </td>
          </tr>           
          @endforeach
          </tbody>
          </table>
    </div>
</div>
@endsection